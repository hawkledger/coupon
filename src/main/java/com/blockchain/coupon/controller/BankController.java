package com.blockchain.coupon.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blockchain.coupon.po.SettlementApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blockchain.coupon.po.BankStaffCustom;
import com.blockchain.coupon.po.MerchantRegisterCustom;
import com.blockchain.coupon.po.SettlementOperationCustom;
import com.blockchain.coupon.service.BankService;

@Controller
@RequestMapping(value="/bank")
public class BankController {
	
	@Autowired
	private BankService bankService;
	
//	结算券申请信息列表查询，是初审和复审列表信息查询入口
	@RequestMapping(value="/querySettlementApplicationList.action",method=RequestMethod.GET)
	public @ResponseBody List<SettlementApplication> querySettlementApplicationList(String id) throws Exception {
		List<SettlementApplication> list=  bankService.querySettlementApplicationList(id);
		return list;
	}
	
//	结算券提现信息列表查询，是初审和复审列表信息查询入口
	@RequestMapping(value="/querySettlementWdList.action",method=RequestMethod.GET)
	public @ResponseBody List<SettlementApplication> querySettlementWdList(String id) throws Exception {
		List<SettlementApplication> list=  bankService.querySettlementWdList(id);
		return list;
	}
	
//	结算券申请初审结果更新
	@RequestMapping(value="/scFirstCheck.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> scFirstCheck(SettlementOperationCustom soc) throws Exception {
		Map<String, String> m=new HashMap<String, String>();
		String str=bankService.updateSettlementFirstCheck(soc);
		m.put("resultCode", str);
		return m;
	}
	
//	结算券申请复审结果更新
	@RequestMapping(value="/scRecheck.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> scRecheck(SettlementOperationCustom soc) throws Exception {
		Map<String, String> m=new HashMap<String, String>();
		String str=bankService.updateSettlementSecondCheck(soc,true);
		m.put("resultCode", str);
		return m;
	}
	
//	结算券提现初审结果更新
	@RequestMapping(value="/wdFirstCheck.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> wdFirstCheck(SettlementOperationCustom soc) throws Exception {
		Map<String, String> m=new HashMap<String, String>();
		String str=bankService.updateSettlementFirstCheck(soc);
		m.put("resultCode", str);
		return m;
	}
	
//	结算券提现复审结果更新
	@RequestMapping(value="/wdRecheck.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> wdRecheck(SettlementOperationCustom soc) throws Exception {
		Map<String, String> m=new HashMap<String, String>();
		String str=bankService.updateSettlementSecondCheck(soc,false);
		m.put("resultCode", str);
		return m;
	}
	
//	查询注册信息尚未审核的商户
	@RequestMapping(value="/queryUncheckMerchants.action",method=RequestMethod.GET)
	public @ResponseBody List<MerchantRegisterCustom> queryUncheckMerchants()throws Exception{
		return bankService.queryUncheckMerchant();
	}

//	商户注册申请审批
	@RequestMapping(value="/updateMerchantRegisterInfo.action",method=RequestMethod.POST)
	public @ResponseBody  List<MerchantRegisterCustom> updateMerchantRegisterInfo(MerchantRegisterCustom merchantRegisterCustom)throws Exception{
		return bankService.updateMerchantRegisterInfo(merchantRegisterCustom);
	}
	
//	银行员工注册
	@RequestMapping(value="/register.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> register(BankStaffCustom bankStaffCustom)throws Exception{
		String resultCode= bankService.insertBankStaff(bankStaffCustom);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;
	}
	
//	银行员工登录
	@RequestMapping(value="/login.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> login(String account,String password)throws Exception{
		return bankService.login(account, password);
	}
}
