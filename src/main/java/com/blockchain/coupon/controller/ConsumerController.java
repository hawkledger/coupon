package com.blockchain.coupon.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blockchain.coupon.po.ConsumerCoupon;
import com.blockchain.coupon.po.ConsumerCustom;
import com.blockchain.coupon.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blockchain.coupon.po.CouponApplicationCustom;

@Controller
@RequestMapping(value="/consumer")
public class ConsumerController {
	
	@Autowired
	private ConsumerService consumerService;
	
//	消费者注册
	@RequestMapping(value="/register.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> register(ConsumerCustom consumerCustom)throws Exception{
		String resultCode=consumerService.consumerRegister(consumerCustom);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;
	}
	

//	消费者登录
	@RequestMapping(value="/login.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> login(String account, String password)throws Exception{
		return consumerService.consumerLogin(account, password);
	}
	
//	消费者优惠券查询
	@RequestMapping(value="/queryUnusedCoupons.action",method=RequestMethod.GET)
	public @ResponseBody List<ConsumerCoupon> queryUnusedCoupons(String id)throws Exception{
		return consumerService.consumerQueryUnusedCoupons(id);
	}
	
//	插如优惠券申请记录
	@RequestMapping(value="/applyCoupon.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> applyCoupon(CouponApplicationCustom couponApplicationCustom)throws Exception{
		String resultCode=consumerService.insertCouponApplication(couponApplicationCustom);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;

	}

	
//	插如优惠券支付申请记录
	@RequestMapping(value="/applyUseCoupon.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> applyUseCoupon(String  merchantId,String consumerId,String couponIds)throws Exception{
		String resultCode=consumerService.insertCouponPayApp(merchantId, consumerId, couponIds);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;
	}
}
