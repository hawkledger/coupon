package com.blockchain.coupon.mapper;

import java.util.List;

import com.blockchain.coupon.po.CouponApplicationCustom;
import com.blockchain.coupon.po.CouponCountInfo;
import com.blockchain.coupon.po.CouponCustom;
import com.blockchain.coupon.po.CouponInfo;
import com.blockchain.coupon.po.CouponPayApplicationCustom;
import com.blockchain.coupon.po.CouponRulerCustom;
import com.blockchain.coupon.po.QueryCouponStatus;

public interface CouponMapper {
	
//	添加优惠券发行规则
	public void insertCouponRuler(CouponRulerCustom couponRulerCustom)throws Exception;
	
//	根据优惠券发行规则id，查询规则详细信息
	public CouponRulerCustom queryCouponRulerInfo(String id)throws Exception;
	
//	插入优惠券
	public void insertCoupon(CouponCustom couponCustom)throws Exception;
	
//	商户查询优惠券状态
	public List<CouponInfo> queryCouponStatus(QueryCouponStatus queryCouponStatus)throws Exception; 
	
//	查询已使用的优惠券总额
	public CouponCountInfo queryTotalUsedCoupons(String id)throws Exception;
	
//	查询未使用的优惠券总额
	public CouponCountInfo queryTotalUnusedCoupons(String id)throws Exception;
	
//	查询优惠券申请信息
	public List<CouponApplicationCustom> queryCouponApplication(String id)throws Exception;
	
//	更新优惠券申请状态
	public void updateCouponAppStatus(CouponApplicationCustom cac)throws Exception;
	
	
//	更新优惠券信息
	 public void updateCouponInfo(CouponCustom couponCustom)throws Exception;
	 
//	 查询优惠券支付申请
	 public List<CouponPayApplicationCustom> queryCouponPayApp(String id)throws Exception;
	 
//	 查询商户尚未发放的优惠券
	public List<CouponCustom> queryUnIssuedCoupons(String id)throws Exception;
	
//	更新优惠券支付申请状态
	public void updateCouponPayApp(String applicationCode)throws Exception;
	
//	根据优惠券支付申请码查询消费者id和优惠券id
	public List<CouponPayApplicationCustom> queryIdByAppCode(String applicationCode)throws Exception;
	
//	根据优惠券id查询优惠券信息
	public void updateCouponInfoById(CouponCustom couponCustom)throws Exception;
	
//	根据优惠券id查询优惠券合约地址
	public String queryConponConAddr(String id)throws Exception;
	
//	插入优惠券申请
	public void insertCouponApplication(CouponApplicationCustom couponApplicationCustom)throws Exception;
	
//	添加优惠券支付申请
	 public void insertCouponPayApp(CouponPayApplicationCustom cpac)throws Exception;
	 
//	 将优惠券的状态改为申请使用中
	 public void updateCouponInUse(String id)throws Exception;
}
