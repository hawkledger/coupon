package com.blockchain.coupon.po;

public class SettlementOperationCustom extends SettlementOperation{
	private String merchantName;
	
	private String bankStaffId;

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getBankStaffId() {
		return bankStaffId;
	}

	public void setBankStaffId(String bankStaffId) {
		this.bankStaffId = bankStaffId;
	}
	
}
