package com.blockchain.coupon.service;

import java.util.List;
import java.util.Map;

import com.blockchain.coupon.po.BankStaffCustom;
import com.blockchain.coupon.po.MerchantRegisterCustom;
import com.blockchain.coupon.po.SettlementApplication;
import com.blockchain.coupon.po.SettlementOperationCustom;

public interface BankService {

//	结算券申请列表查询
	public  List<SettlementApplication> querySettlementApplicationList(String id)throws Exception;
	
//	结算券提现申请列表查询
	public  List<SettlementApplication> querySettlementWdList(String id)throws Exception;
	
//	更新结算券初审结果
	public String updateSettlementFirstCheck(SettlementOperationCustom settlementOperationCustom)throws Exception;
	
//	更新结算券复审结果
	public String updateSettlementSecondCheck(SettlementOperationCustom settlementOperationCustoms,boolean idAdd)throws Exception;
	
//	查询注册信息尚未审核的商户
	public List<MerchantRegisterCustom> queryUncheckMerchant()throws Exception;
	
//  更新注册待审核的商户信息
	public List<MerchantRegisterCustom> updateMerchantRegisterInfo(MerchantRegisterCustom merchantRegisterCustom)throws Exception;
	
//	注册员工
	public String insertBankStaff(BankStaffCustom bankStaffCustom)throws Exception;
	
//	员工登录
	public Map<String, String> login(String account,String password)throws Exception;
}
