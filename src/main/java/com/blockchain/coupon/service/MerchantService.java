package com.blockchain.coupon.service;

import java.util.List;
import java.util.Map;

import com.blockchain.coupon.po.CouponApplicationCustom;
import com.blockchain.coupon.po.CouponCountInfo;
import com.blockchain.coupon.po.CouponInfo;
import com.blockchain.coupon.po.CouponPayApplicationCustom;
import com.blockchain.coupon.po.CouponRulerCustom;
import com.blockchain.coupon.po.MerchantRegisterCustom;
import com.blockchain.coupon.po.QueryCouponStatus;
import com.blockchain.coupon.po.SettlementOperationCustom;

public interface MerchantService {

//	结算券申请
	public String insertSettlementOperation(SettlementOperationCustom settlementOperationCustom)throws Exception;

//	查询商户结算券余额
	public Integer querySettlementBalance(String merchantId)throws Exception;
	
//	商户注册
	public String insertMerchant(MerchantRegisterCustom merchantRegisterCustom)throws Exception;
	
//	商户登录
	public Map<String, String> login(String account,String password)throws Exception;
	
//	添加优惠券发行规则
	public String insertCouponRuler(CouponRulerCustom couponRulerCustom)throws Exception;
	
//	商户查询优惠券状态
	public List<CouponInfo> queryCouponStatus(QueryCouponStatus queryCouponStatus)throws Exception; 
	
//	查询已使用和未使用优惠券总额
	public CouponCountInfo queryTotalCoupons(String id)throws Exception;
	
//	查询优惠券申请信息
	public List<CouponApplicationCustom> queryCouponApplication(String id)throws Exception;
	
//	计算消费者某笔消费获取的优惠券额度
	public Map<String, Integer> getCouponValue(String merchantId,String consumptionValue)throws Exception;
	
//	优惠券申请处理
	public String dealCouponApplicaiton(CouponApplicationCustom cac)throws Exception;
	
//	 查询优惠券支付申请
	 public List<CouponPayApplicationCustom> queryCouponPayApp(String id)throws Exception;
	 
//	 优惠券支付支付确认
	 public String dealCouponPayApp(String id,String applicationCode)throws Exception;
}
