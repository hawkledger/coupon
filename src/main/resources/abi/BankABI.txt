[{
    constant: false,
    inputs: [{
        name: "signature",
        type: "bytes"
    }, {
        name: "_merchant",
        type: "address"
    }, {
        name: "value",
        type: "uint256"
    }, {
        name: "isAdd",
        type: "bool"
    }, {
        name: "sender",
        type: "address"
    }],
    name: "approve",
    outputs: [],
    type: "function"
}, {
    constant: false,
    inputs: [{
        name: "_issuePermission",
        type: "bool"
    }, {
        name: "_chargePermission",
        type: "bool"
    }, {
        name: "_merchant",
        type: "address"
    }, {
        name: "sender",
        type: "address"
    }],
    name: "setPermissions",
    outputs: [],
    type: "function"
}, {
    constant: false,
    inputs: [{
        name: "signature",
        type: "bytes"
    }],
    name: "check",
    outputs: [{
        name: "",
        type: "bool"
    }],
    type: "function"
}, {
    inputs: [{
        name: "opKey",
        type: "address"
    }, {
        name: "ckKey",
        type: "address"
    }, {
        name: "sender",
        type: "address"
    }],
    type: "constructor"
}]