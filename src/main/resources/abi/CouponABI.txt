[{
    constant: false,
    inputs: [{
        name: "date",
        type: "bytes"
    }, {
        name: "consumer",
        type: "address"
    }, {
        name: "amount",
        type: "uint256"
    }],
    name: "consumeRecording",
    outputs: [],
    type: "function"
}, {
    constant: true,
    inputs: [],
    name: "getState",
    outputs: [{
        name: "",
        type: "uint8"
    }],
    type: "function"
}, {
    constant: false,
    inputs: [{
        name: "receiver",
        type: "address"
    }],
    name: "transfer",
    outputs: [],
    type: "function"
}, {
    constant: true,
    inputs: [],
    name: "getValue",
    outputs: [{
        name: "",
        type: "uint256"
    }],
    type: "function"
}, {
    constant: false,
    inputs: [{
        name: "merchant",
        type: "address"
    }],
    name: "payApply",
    outputs: [],
    type: "function"
}, {
    constant: false,
    inputs: [{
        name: "st",
        type: "uint8"
    }],
    name: "setState",
    outputs: [],
    type: "function"
}, {
    constant: true,
    inputs: [],
    name: "getRecording",
    outputs: [{
        name: "",
        type: "bytes"
    }, {
        name: "",
        type: "address"
    }, {
        name: "",
        type: "uint256"
    }],
    type: "function"
}, {
    inputs: [{
        name: "_value",
        type: "uint256"
    }, {
        name: "_validStart",
        type: "bytes"
    }, {
        name: "_validEnd",
        type: "bytes"
    }, {
        name: "_publisher",
        type: "address"
    }],
    type: "constructor"
}]